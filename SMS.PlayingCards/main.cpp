//Playing Cards
//Simon
// Logan C

#include<iostream>
#include<conio.h>
#include <ctime>

using namespace std;

enum class Suit
{
	Clubs,
	Hearts,
	Spades,
	Diamonds
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Suit Suit;
	Rank Rank;
	
};

void PrintCard(Card card)
{
	// I was able to get it to print out the higher card but it doesnt print out the suit name or 
	// the rank name it just gives me the numbers corresponding to them and I'm not sure how to fix that
	cout << "The " << (int)card.Rank << " of " << (int)card.Suit;
}

Card HighCard(Card c1, Card c2) 
{
	
	if (c1.Rank > c2.Rank)
	{
		return c1;
	}
	else
	{
		return c2;
	}
}

int main()
{
	Card c1;
	Card c2;

	c1.Rank = Rank(rand() % 15);
	c1.Suit = Suit(rand() % 4);
	c2.Rank = Rank(rand() % 15);
	c2.Suit = Suit(rand() % 4);

	PrintCard(HighCard(c1, c2));

	(void)_getch();
	return 0;
}